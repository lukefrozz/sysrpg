package sysRPG.control;

import java.util.ArrayList;
import java.util.List;

import sysRPG.domains.*;
/**
 * Classe que contem colecoes padroes
 * de dados
 * @author Fabio Onofre
 * @since 30/09/2015
 */
public class DefaultCollection {
	
	/**
	 * Colecao padrao de racas
	 * @return Lista de Racas
	 */
	public static List<Raca> racas(){
		
		// Cole��o de dados referente as Ra�as
		List<Raca> racas = new ArrayList<>();
		
		// An�o
		Tendencia anaoT = new Tendencia(Moral.Leal, Etica.Neutro);
		
		Atributos anaoAT = new Atributos(0, -2, 0, 0, 2, 0);
		
		Raca anao = new Raca("An�o", Tamanho.M, anaoT, anaoAT, 0);

		racas.add(anao);
		
		//Elfo
		Tendencia elfoT = new Tendencia(Moral.Caotico, Etica.Bom);
		
		Atributos elfoAT = new Atributos(0, 4, -2, 2, 0, 0);
		
		Raca elfo = new Raca("Elfo", Tamanho.M, elfoT, elfoAT, 0);
		
		racas.add(elfo);
		
		// Goblin
		Tendencia goblinT = new Tendencia(Moral.Caotico, Etica.Neutro);
		
		Atributos goblinAT = new Atributos(0, 4, 2, 0, 0,-2);
		
		Raca goblin = new Raca("Goblin", Tamanho.P, goblinT, goblinAT, 0);
		
		racas.add(goblin);
		
		//Halfling
		Tendencia halflingT = new Tendencia(Moral.Caotico, Etica.Bom);
		
		Atributos halflingAT = new Atributos(-2, 4, 0, 0, 0, 2);
		
		Raca halfling = new Raca("Halfling", Tamanho.P, halflingT, halflingAT, 0);
		
		racas.add(halfling);
		
		// Humano
		Raca humano = new Raca();
		
		humano.setNome("Humano");
		humano.setTamanhoPadrao(Tamanho.M);
		humano.setQtdCustom(2);
		
		Raca lefou = new Raca();
		
		// Lefou
		lefou.setNome("Lefou");
		lefou.setTamanhoPadrao(Tamanho.M);
		
		Tendencia lefouT = new Tendencia();
		lefouT.setMoral(Moral.Caotico);
		lefouT.setEtica(Etica.Neutro);
		lefou.setTendenciaPadrao(lefouT);
		
		Atributos lefouAT = new Atributos();
		
		racas.add(lefou);
		
		// Minotauro
		Tendencia minotauroT = new Tendencia(Moral.Leal, Etica.Neutro);
		
		Atributos minotauroAT = new Atributos(4, 0, 2, 0, 0, -2);
		
		Raca minotauro = new Raca("Minotauro", Tamanho.M, minotauroT, minotauroAT, 0);
		
		racas.add(minotauro);
		
		//Qareen
		Tendencia qareenT = new Tendencia(Moral.Leal, Etica.Bom);
		
		Atributos qareenAT = new Atributos(0, 0, 0, 2, -2, 4);
		
		Raca qareen = new Raca("Qareen", Tamanho.M, qareenT, qareenAT, 0);
		
		racas.add(qareen);
		
		return racas;
		
	}
	
	public static List<Divindade> divindades() {
		List<Divindade> ds = new ArrayList<>();
		
		// 1: Allihanna - Deusa da Natureza
		ds.add(new Divindade("Deusa da Natureza", 
			new Deus("Allihanna", 
				new Tendencia(Moral.Neutro, Etica.Bom),
				"Bord�o"),
			"Plantas e Animais"));
		
		// 2: Azger - Deus-Sol
		ds.add(new Divindade("Deus-Sol", 
			new Deus("Arzger", 
				new Tendencia(Moral.Leal, Etica.Bom),
					"Cimitarra"),
			"Sol e Dia"));		

		// 3: Hyninn - Deus da Trapa�a
		ds.add(new Divindade("Deus da Trapa�a", 
			new Deus("Hyninn", 
				new Tendencia(Moral.Caotico, Etica.Neutro),
				"Adaga"),
			"Ladr�es"));

		// 4: Kallyadranoch - Deus dos Drag�es
		ds.add(new Divindade("Deus dos Drag�es", 
			new Deus("Kallyadranoch", 
				new Tendencia(Moral.Leal, Etica.Mal),
				"Lan�a"),
			"Drag�es"));
		
		// 5: Keen - Deus da Guerra
		ds.add(new Divindade("Deus da Guerra", 
			new Deus("Keen", 
				new Tendencia(Moral.Caotico, Etica.Mal),
					"Machado de Batalha"),
			"Guerras"));		
		
		// 6: Khalamyr - Deus da Justi�a
		ds.add(new Divindade("Deus da Justi�a", 
			new Deus("Khalamyr", 
				new Tendencia(Moral.Leal, Etica.Bom),
				"Espada Longa"),
			"Justi�a"));

		// 7: Lena - Deusa da Fertilidade e Cura
		ds.add(new Divindade("Deusa da Vida", 
			new Deus("Lena", 
				new Tendencia(Moral.Neutro, Etica.Bom),
				"N/A"),
			"Fertilidade/Cura"));
		
		// 8: LinWu - Deus-Drag�o
		ds.add(new Divindade("Deus-Drag�o", 
			new Deus("Lin-Wu", 
				new Tendencia(Moral.Leal, Etica.Neutro),
					"Katana"),
			// TODO definir a area de atua��o de Lin-Wu
			"---"));		

		// 9: Marah - Deusa da Paz
		ds.add(new Divindade("Deusa da Paz", 
			new Deus("Marah", 
				new Tendencia(Moral.Neutro, Etica.Bom),
				"N/A"),
			"Amor e Alegria"));

		// 10: Megalokk - Deus dos Monstros
		ds.add(new Divindade("Deus dos Monstros", 
			new Deus("Megalokk", 
				new Tendencia(Moral.Caotico, Etica.Mal),
				"Lan�a"),
			"Ma�a"));
		
		// 11: Nimb - Deus do Caos
		ds.add(new Divindade("Deus do Caos", 
			new Deus("Nimb", 
				new Tendencia(Moral.Caotico, Etica.Neutro),
					"Nenhuma e Todas"),
			"Sorte e Azar"));		
		
		// 12: Oceano - Deus dos Mares
		ds.add(new Divindade("Deus dos Mares", 
			new Deus("Oceano", 
				new Tendencia(Moral.Neutro, Etica.Neutro),
				"Tridente"),
			"Mar"));
		
		// 13: Ragnar - Deus da Morte
		ds.add(new Divindade("Deusa da Morte", 
			new Deus("Ragnar", 
				new Tendencia(Moral.Caotico, Etica.Mal),
				"Ma�a"),
			"Morte"));
		
		// 14: Sszzaas - Deus da Trai��o
		ds.add(new Divindade("Deus da Trai��o", 
			new Deus("Sszzaas", 
				new Tendencia(Moral.Neutro, Etica.Mal),
					"Adaga"),
			"Trai��o"));		

		// 15: Tanna-Toh - Deusa do Conhecimento
		ds.add(new Divindade("Deus do Conhecimento", 
			new Deus("Tanna-Toh", 
				new Tendencia(Moral.Leal, Etica.Neutro),
				"Bord�o"),
			"Conhecimento"));

		// 16: Tenebra - Deusa das Trevas
		ds.add(new Divindade("Deusa das Trevas", 
			new Deus("Tenebra", 
				new Tendencia(Moral.Neutro, Etica.Neutro),
				"Shuriken"),
			"Trevas e Noite"));
		
		// 17: Thyatis - Deus da Ressurei��o e Profecias
		ds.add(new Divindade("Deus da Ressurei��o", 
			new Deus("Thyatis", 
				new Tendencia(Moral.Leal, Etica.Bom),
					"Espada longa"),
			"Ressurrei��o"));		
		
		// 18: Wynna - Deusa da Magia
		ds.add(new Divindade("Deus da Magia", 
			new Deus("Wynna", 
				new Tendencia(Moral.Caotico, Etica.Neutro),
				"Adaga"),
			"Magia"));

		// 19: Valkaria - Deusa da Ambi��o
		ds.add(new Divindade("Deusa da Ambi��o", 
			new Deus("Valkaria", 
				new Tendencia(Moral.Caotico, Etica.Bom),
				"Mangual"),
			"Ambi��o"));
		
		return ds;
	}

}
