package sysRPG.control;

import java.util.*;

import sysRPG.domains.*;

/**
 * Classe para a instancia��o de objetos padr�o
 * para testes r�pidos
 * @author Luke Frozz
 * @since 29/09/2015
 */
public class ObjectPattern {
	/**
	 * Template de personagem
	 * @return personagem de teste
	 */
	public static Personagem personagemTemplate() {
		Personagem p = new Personagem();
		
		p.setNome("Lucius");
		p.setIdade(150);
		p.setAtributos(atributosPatternPersonagem());
		p.setRaca(racaPattern());
		p.setDivindade(divindadePattern());
		p.setSexo(Sexo.Masculino);
		
		return p;
	}
	/**
	 * Template de atributos de Personagem
	 * @return atributos do personagem padr�o
	 */
	public static Atributos atributosPatternPersonagem() {
		Atributos a = new Atributos();
		
		a.setForca(12);
		a.setDestreza(15);
		a.setConstituicao(10);
		a.setInteligencia(14);
		a.setSabedoria(13);
		a.setCarisma(16);
		
		return a;
	}
	/**
	 * Template de raca
	 * @return raca padrao do personagem
	 */
	public static Raca racaPattern() {
		Raca r = new Raca();
		
		r.setNome("Elfo");
		r.setTamanhoPadrao(Tamanho.M);
		r.setTendenciaPadrao(tendenciaPatternRaca());
		r.setAlteracaoAtributos(atributosPatternRaca());
		
		return r;
	}
	/**
	 * Template de tendencia
	 * @return tendencia padrao da raca
	 */
	public static Tendencia tendenciaPatternRaca() {
		Tendencia t = new Tendencia();
		
		t.setMoral(Moral.Caotico);
		t.setEtica(Etica.Bom);
		
		return t;
	}
	/**
	 * Template de atributos da raca
	 * @return modificadores padrao da Raca
	 */
	public static Atributos atributosPatternRaca() {
		
		Atributos a = new Atributos();
		
		a.setForca(0);
		a.setDestreza(4);
		a.setConstituicao(-2);
		a.setInteligencia(2);
		a.setSabedoria(0);
		a.setCarisma(0);
		
		return a;
	}
	/**
	 * Template de Divindade
	 * @return divindade padrao da Personagem
	 */
	public static Divindade divindadePattern() {
		Divindade d = new Divindade();
		
		d.setNome("Deusa da Natureza");
		d.setCampoAcao("Plantas e Animais");
		d.setDeus(deusPattern());
		return d;
	}
	/**
	 * Template de deus
	 * @return deus padrao da divindade
	 */
	public static Deus deusPattern() {
		Deus d = new Deus();
		
		d.setNome("Allihanna");
		d.setTendencia(tendenciaPatternDeus());
		d.setArmaPreferida(armaPattern());
		d.setRacasTipicas(racasTipicasPattern());
		d.setClassesTipicas(classesTipicasPattern());
		return d;
	}
	/**
	 * Template de tendencia do deus
	 * @return tendencia padrao do deus
	 */
	public static Tendencia tendenciaPatternDeus() {
		Tendencia t = new Tendencia();
		
		t.setMoral(Moral.Neutro);
		t.setEtica(Etica.Bom);
		
		return t;
	}
	/**
	 * Template de arma
	 * @return arma padrao do deus
	 */
	public static Arma armaPattern() {
		Arma a = new Arma();
		
		a.setNome("Bord�o");
		
		return a;
	}
	/**
	 * template de racas adoradoras
	 * @return radas adoradoras padrao do deus
	 */
	public static List<Raca> racasTipicasPattern() {
		List<Raca> at = new ArrayList<>();
		
		Raca r1 = new Raca();
		r1.setNome("Elfo");
		at.add(r1);
		
		Raca r2 = new Raca();
		r2.setNome("Halfling");
		at.add(r2);
				
		return at;
	}
	/**
	 * template padrao de classes adoradoras
	 * @return classes adoradoras do deus
	 */
	public static List<Classe> classesTipicasPattern() {
		List<Classe> ct = new ArrayList<>();
		
		Classe c1 = new Classe();
		c1.setNome("Barbaro");
		ct.add(c1);
		
		Classe c2 = new Classe();
		c2.setNome("Druida");
		ct.add(c2);
		
		Classe c3 = new Classe();
		c3.setNome("Ranger");
		ct.add(c3);

		return ct;
	}
}
