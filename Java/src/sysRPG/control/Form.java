package sysRPG.control;

import java.util.Scanner;

import sysRPG.domains.*;

/**
 * Classe de formulários de instanciação
 * geral de objetos
 * @author Fábio Onofre
 * @since 25/09/2015
 */
public class Form {
	/**
	 * Scanner utilizado para puxar as informações
	 */
	private static Scanner entrada = new Scanner(System.in);
	
	/**
	 * Método para criar novo
	 * objeto de atributo de habilidades
	 * @return novo Atributos
	 */
	public static Atributos novoAtributos(){
		
		//entrada = new Scanner(System.in);
		
		Atributos a = new Atributos();
		
		System.out.print("Digite a força: ");
		a.setForca(entrada.nextInt());
		System.out.print("Digite a destreza: ");
		a.setDestreza(entrada.nextInt());
		System.out.print("Digite a constituição: ");
		a.setConstituicao(entrada.nextInt());
		System.out.print("Digite a inteligência: ");
		a.setInteligencia(entrada.nextInt());
		System.out.print("Digite a sabedoria: ");
		a.setSabedoria(entrada.nextInt());
		System.out.print("Digite o carisma: ");
		a.setCarisma(entrada.nextInt());
		
		//entrada.close();
		
		return a;
		
	}
	
	/**
	 * Método para instanciar nova tendência
	 * @return Nova tendência
	 */
	public static Tendencia novaTendencia(){

		//entrada = new Scanner(System.in);
		
		Tendencia t = new Tendencia();
		
		/*System.out.println("Digite o nome da tendкncia em portuguкs\n");
		t.setNomePT(entrada.nextLine());
		System.out.println("Digite o nome da tendкncia em inglкs\n");
		t.setNomeING(entrada.nextLine());
		System.out.println("Digite a descriзгo da tendкncia\n");
		t.setDescricao(entrada.nextLine());*/
		System.out.println("Informe o índice de moral: ");
		System.out.println("1 - Leal");
		System.out.println("2 - Neutro");
		System.out.println("3 - Caótico");
		System.out.print("Sua opção: ");
		int op = entrada.nextInt();
		if (op == 1)
			t.setMoral(Moral.Leal);
		else if (op == 2)
			t.setMoral(Moral.Neutro);
		else if (op == 3)
			t.setMoral(Moral.Caotico);
		System.out.println("Informe o índice de ética: ");
		System.out.println("1 - Bom");
		System.out.println("2 - Neutro");
		System.out.println("3 - Mal");
		System.out.print("Sua opção: ");
		op = entrada.nextInt();
		if (op == 1)
			t.setEtica(Etica.Bom);
		else if (op == 2)
			t.setEtica(Etica.Neutro);
		else if (op == 3)
			t.setEtica(Etica.Mal);
		
		//entrada.close();
		
		return t;
		
	}
	
	/**
	 * Método para instanciar nova Raça
	 * @return um objeto do tipo Raça
	 */
	public static Raca novaRaca(){

		//entrada = new Scanner(System.in);
		
		Raca r = new Raca();
		
		System.out.print("Digite o nome da raça: ");
		entrada.nextLine();
		r.setNome(entrada.next());
		//System.out.print("Informe a descrição da raça: ");
		//r.setDescricao(entrada.next());
		System.out.println("Defina o tamanho da raça: ");
		System.out.println("P  - Pequeno");
		System.out.println("M  - Medio");
		System.out.println("G  - Grande");
		System.out.println("GG - Gigante");
		System.out.println("E  - Enorme");
		System.out.println("C  - Colossal");
		System.out.print("Sua opção: ");
		entrada.nextLine();
		r.setTamanhoPadrao(Tamanho.valueOf(entrada.next().toUpperCase()));
		System.out.println("Defina os atributos de habilidade da raça: ");
		r.setAlteracaoAtributos(novoAtributos());
		r.setTendenciaPadrao(novaTendencia());

		//entrada.close();
		
		return r;
		
	}
	
	/**
	 * Método para instanciar nova Perícia
	 * @return uma Perícia
	 */
	public static Pericia novaPericia(){

		//entrada = new Scanner(System.in);

		Pericia p = new Pericia();
		
		System.out.print("Digite o nome da pericia: ");
		p.setNome(entrada.nextLine());
		System.out.print("Informe a descriзaх da pericia: ");
		p.setDescricao(entrada.nextLine());
		
		//entrada.close();

		return p;
		
	}
	
	/**
	 * Método para instanciar uma 
	 * nova Personagem
	 * @return nova Personagem
	 */
	public static Personagem novoPersonagem(){
		
		//entrada = new Scanner(System.in);

		Personagem p = new Personagem();
		
		System.out.print("Digite o nome: ");
		p.setNome(entrada.nextLine());
		System.out.print("Digite a idade: ");
		p.setIdade(entrada.nextInt());
		System.out.println("Digite o sexo ");
		System.out.println("1 - Masculino");
		System.out.println("2 - Feminino");
		System.out.print("Sua opção: ");
		int i = entrada.nextInt();
		p.setSexo((i == 1) ? Sexo.Masculino : Sexo.Feminino);
		System.out.println("Defina os atributos de habilidade da personagem: ");
		p.setAtributos(novoAtributos());
		p.setRaca(defineRaca());
		p.setDivindade(defineDivindade());
		
		
		//entrada.close();

		return p;
		
	}
	
	/**
	 * Método para instanciar 
	 * um novo Deus
	 * @return Objeto do tipo Deus
	 */
	public static Deus novoDeus(){
		
		Deus d = new Deus();
		entrada.nextLine();
		System.out.print("Digite o nome do Deus: ");
		d.setNome(entrada.nextLine());
		System.out.println("Informe a tendência do Deus: ");
		d.setTendencia(novaTendencia());
		System.out.println("Informe a arma preferida do Deus: ");
		d.setArmaPreferida(novaArma());
		
		return d;
	}
	
	/**
	 * Método para instancia
	 * uma nova arma
	 * @return Objeto do tipo Arma
	 */
	public static Arma novaArma(){
		
		Arma ar = new Arma();
		
		System.out.print("Digite o nome da arma: ");
		ar.setNome(entrada.next());
		
		return ar;
	}
	
	/**
	 * Método para instanciar
	 * uma nova divindade
	 * @return Objeto do tipo Divindade
	 */
	public static Divindade novaDivindade(){
		
		Divindade div = new Divindade();
		
		System.out.print("Digite o nome da divindade: ");
		div.setNome(entrada.next());
		System.out.println("Dê informações sobre o Deus de "+div.getNome()+":");
		div.setDeus(novoDeus());
		System.out.print("Informe o campo de ação: ");
		div.setCampoAcao(entrada.next());
		
		return div;
		
	}
	/**
	 * Método de definição de Raça
	 * @return raça definida e/ou escolhida
	 */
	public static Raca defineRaca() {
		System.out.println("Defina a raça da personagem selecionando seu número no índice ou digite 0 para criar uma nova");
		System.out.print(Views.racas());
		System.out.print("Sua opção: ");
		int op = entrada.nextInt();
		if (op == 0)
			return novaRaca();
		else
			return DefaultCollection.racas().get(op-1);
	}
	
	public static Divindade defineDivindade() {
		System.out.println("Defina a divindade que a personagem seguirá selecionando seu número no índice ou digite 0 para criar uma nova");
		System.out.print(Views.divindades());
		System.out.print("Sua opção: ");
		int op = entrada.nextInt();
		if (op == 0)
			return novaDivindade();
		else
			return DefaultCollection.divindades().get(op-1);
		
	}
}
