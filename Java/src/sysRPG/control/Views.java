/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.control;

import java.util.List;

import sysRPG.domains.*;

/**
 * Classe de visualizacoes das informacoes
 * de todas as classes
 * @author Luke Frozz
 * @since 24/09/2015
 */
public class Views {
    /**
     * Visualizacao do Personagem com atributos
     * comum
     * @param p Personagem a ser visualizado
     */
    public static String personagemPure(Personagem p) {
        StringBuilder sb = new StringBuilder();
        sb.append("|--------------------------------|\n");
        sb.append(String.format("|Personagem: %20s|\n", p.getNome()));
        sb.append("|--------------------------------|\n");
        sb.append(String.format("|Idade: %25s|\n", (
            p.getIdade() == null ? "desconhecida" : p.getIdade())));
        sb.append(String.format("|Sexo: %9s|Tamanho: %7s|\n", 
    		p.getSexo(), p.getRaca().getTamanhoPadrao()));
        sb.append(String.format("|Tend�ncia: %21s|\n", 
            tendenciaPure(p.getTendencia())));
        /*
        sb.append(String.format("|%32s|\n", 
            String.format("%s / %s",
                p.getTendencia().getNomePT(),
                p.getTendencia().getNomeING())));
        */
        sb.append(String.format("|Ra�a: %26s|\n", p.getRaca().getNome()));
        sb.append(divindadePure(p.getDivindade()));
        sb.append("|------------ATRIBUTOS-----------|\n");
        sb.append("|-NOMEC-|--VLR--|--RA�A--|--MOD--|\n");
        sb.append(String.format("|  FOR%8s%9s%8s%3s\n", 
    		p.getAtributos().getForca(),
    		p.getRaca().getAlteracaoAtributos().getForca(),
    		Atributos.mod(p.getAtributos().getForca() + 
				p.getRaca().getAlteracaoAtributos().getForca()),
    		"|"));
        sb.append(String.format("|  DES%8s%9s%8s%3s\n", 
    		p.getAtributos().getDestreza(),
    		p.getRaca().getAlteracaoAtributos().getDestreza(),
    		Atributos.mod(p.getAtributos().getDestreza() + 
				p.getRaca().getAlteracaoAtributos().getDestreza()),
    		"|"));
        sb.append(String.format("|  CON%8s%9s%8s%3s\n", 
    		p.getAtributos().getConstituicao(),
    		p.getRaca().getAlteracaoAtributos().getConstituicao(),
    		Atributos.mod(p.getAtributos().getConstituicao() + 
				p.getRaca().getAlteracaoAtributos().getConstituicao()),
    		"|"));
        sb.append(String.format("|  INT%8s%9s%8s%3s\n", 
    		p.getAtributos().getInteligencia(),
    		p.getRaca().getAlteracaoAtributos().getInteligencia(),
    		Atributos.mod(p.getAtributos().getInteligencia() + 
				p.getRaca().getAlteracaoAtributos().getInteligencia()),
    		"|"));
        sb.append(String.format("|  SAB%8s%9s%8s%3s\n", 
    		p.getAtributos().getSabedoria(),
    		p.getRaca().getAlteracaoAtributos().getSabedoria(),
    		Atributos.mod(p.getAtributos().getSabedoria() + 
				p.getRaca().getAlteracaoAtributos().getSabedoria()),
    		"|"));
        sb.append(String.format("|  CAR%8s%9s%8s%3s\n", 
    		p.getAtributos().getCarisma(),
    		p.getRaca().getAlteracaoAtributos().getCarisma(),
    		Atributos.mod(p.getAtributos().getCarisma() + 
				p.getRaca().getAlteracaoAtributos().getCarisma()),
    		"|"));
        sb.append("|--------------------------------|\n");
        
        return sb.toString();
    }
    /**
     * M�todo que retorna um relat�rio completo
     * sobre um deus
     * @param d deus o qual sr� emitido um relat�rio
     * @return relat�rio de informa��es do deus
     */
    public static String deusPure(Deus d) {
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(deusSimple(d));
        sb.append("|_Ra�a de adoradores t�picos_____|\n");
    	if (d.getRacasTipicas().size() >=1){
    		sb.append("|                                |\n");
    		for (Raca r : d.getRacasTipicas())
    			sb.append(String.format("|- %-30s|\n", r.getNome()));
    	} else
    		sb.append("|                                |\n"
    				+ "| N�o h� ra�as de adoradores     |\n"
    				+ "| especificados                  |\n");
        sb.append("|--------------------------------|\n");
        sb.append("|_Classes de adoradores t�picos__|\n");
    	if (d.getClassesTipicas().size() >=1) {
    		sb.append("|                                |\n");
    		for (Classe c : d.getClassesTipicas())
    			sb.append(String.format("|- %-30s|\n", c.getNome()));
    	} else
    		sb.append("|                                |\n"
    				+ "| N�o h� classes de adoradores   |\n"
    				+ "| especificados                  |\n");
        sb.append("|--------------------------------|\n");
        
    	return sb.toString();
    }
    /**
     * M�todo que retorna as informa��es b�sicas sobre
     * um deus
     * @param d deus a ser mostrado as inform��es
     * @return relat�rio basico do deus
     */
    public static String deusSimple(Deus d) {
    	StringBuilder sb = new StringBuilder();
    	
        sb.append("|--------------------------------|\n");
    	sb.append(String.format("|Deus: %26s|\n", d.getNome()));
        sb.append(String.format("|Tend�ncia: %21s|\n", 
            tendenciaPure(d.getTendencia())));
        sb.append(String.format("|Arma Preferida: %16s|\n", 
    		d.getArmaPreferida().getNome()));
        sb.append("|--------------------------------|\n");

        return sb.toString();
    }
    /**
     * M�todo de relat�rio que informa todas as informa��es base
     * de uma divindade
     * @param d par�metro para gerar as informa��es
     * @return relat�rio sobre a divindade
     */
    public static String divindadePure(Divindade d) {
    	StringBuilder sb = new StringBuilder();
    	
        sb.append("|--------------------------------|\n");
        sb.append(String.format("|Divindade: %21s|\n", 
        		d.getNome()));
    	sb.append(String.format("|Campo de a��o: %17s|\n", d.getCampoAcao()));
        sb.append(deusSimple(d.getDeus()));
    	return sb.toString();
    }
    /**
     * M�todo que imprime uma lista com as especifica��es b�sicas de todas as Ra�as
     * @return Relat�rio base das ra�as
     */
    public static String racas() {
    	StringBuilder sb = new StringBuilder();
    	int i = 0;
        sb.append("|--------------------------------|\n");
    	sb.append("|_Ra�as__________________________|\n");
        sb.append("|--------------------------------|\n");
    	for( Raca r : DefaultCollection.racas()) {
    		i++;
            sb.append("|                                |\n");
    		int faltante = 33 - String.format("|_%s_%s", i, r.getNome()).length();
    		sb.append(String.format("|_%s_%s", i, r.getNome()));
    		while (faltante > 0) {
    			sb.append("_");
    			faltante--;
    		}
    		sb.append("|\n");
            sb.append("|                                |\n");
    		sb.append(String.format("|Tend�ncia: %21s|\n", 
				tendenciaPure(r.getTendenciaPadrao())));
    		sb.append(String.format("|%-32s|\n", "Alteradores de habilidade"));
    		sb.append(String.format("|FOR %12s|INT %11s|\n", 
				r.getAlteracaoAtributos().getForca(),
				r.getAlteracaoAtributos().getInteligencia()));
    		sb.append(String.format("|DES %12s|SAB %11s|\n", 
				r.getAlteracaoAtributos().getDestreza(),
				r.getAlteracaoAtributos().getSabedoria()));
    		sb.append(String.format("|CON %12s|CAR %11s|\n", 
				r.getAlteracaoAtributos().getConstituicao(),
				r.getAlteracaoAtributos().getCarisma()));
            sb.append("|--------------------------------|\n");    		
    	}
    	return sb.toString();
    }
    
	public static String divindades() {
		StringBuilder sb = new StringBuilder();
		
    	Integer i = 0;
        sb.append("|--------------------------------|\n");
    	sb.append("|_Divindades_____________________|\n");
        sb.append("|--------------------------------|\n");
    	for( Divindade d : DefaultCollection.divindades()) {
    		i++;
            sb.append("|                                |\n");
    		int faltante = 33 - String.format("|_%s_%s", i, d.getNome()).length();
    		sb.append(String.format("|_%s_%s", i, d.getNome()));
    		while (faltante > 0) {
    			sb.append("_");
    			faltante--;
    		}
    		sb.append("|\n");
            sb.append("|                                |\n");
    		sb.append(String.format("|Campo de a��o: %17s|\n", d.getCampoAcao()));
            sb.append("|                                |\n");
    		sb.append(String.format("|Deus: %26s|\n", d.getDeus().getNome()));
    		sb.append(String.format("|Tend�ncia: %21s|\n", 
				tendenciaPure(d.getDeus().getTendencia())));
    		sb.append(String.format("|Arma: %26s|\n", 
				d.getDeus().getArmaPreferida().getNome()));
            sb.append("|                                |\n");
            sb.append("|--------------------------------|\n");
    	}		
		
		return sb.toString();
	}
	
	/**
	 * Metodo de visualizacao de tendencia
	 * @param t tendencia a ser visualizada
	 * @return Texto formatado refentende aos eixos da tendencia
	 */
	public static String tendenciaPure(Tendencia t) {
		if (t.getMoral().equals(Moral.Neutro) && t.getEtica().equals(Etica.Neutro))
			return "Neutro";
		else
			return String.format("%s e %s", t.getMoral(), t.getEtica());
	}

}
