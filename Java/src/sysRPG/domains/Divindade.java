package sysRPG.domains;
/**
 * Classe representativa da Divindade
 * @author Luke Frozz
 * @since 28/09/2015
 */
public class Divindade {
	
	/**
	 * Id da Divindade
	 */
	private Long id;
	
	/**
	 * Nome da Divindade
	 */
	private String nome;
	
	/**
	 * Deus da Divindade
	 */
	private Deus deus;
	
	/**
	 * Campo de A��o da Divindade
	 */
	private String campoAcao;
	
	/**
	 * Getter de Id
	 * @return Id da Divindade
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Setter de Id
	 * @param id Atribui o Id da Divindade
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Getter de Nome
	 * @return Nome da Divindade
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Setter de Nome
	 * @param nome Atribui o Nome da Divindade
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Getter de Deus
	 * @return Deus da Divindade
	 */
	public Deus getDeus() {
		return deus;
	}
	
	/**
	 * Setter de Deus
	 * @param deus Atribui o Deus da Divindade
	 */
	public void setDeus(Deus deus) {
		this.deus = deus;
	}
	
	/**
	 * Getter de Campo de A��o
	 * @return Campo de A�ao da Divindade
	 */
	public String getCampoAcao() {
		return campoAcao;
	}
	
	/**
	 * Setter de Campo de A��o
	 * @param campoAcao Atribui o Campo de A��o da Divindade
	 */
	public void setCampoAcao(String campoAcao) {
		this.campoAcao = campoAcao;
	}
	
	/**
	 * Construtor padr�o de Divindade
	 */
	public Divindade() {
		this.deus = new Deus();
	}
	
	/**
	 * Construtor completo de Divindade
	 * @param nome atribui o nome da divindade
	 * @param deus atribui o deus da divindade
	 * @param campoAcao atribui o campo de a��o da divindade
	 */
	public Divindade(String nome, Deus deus, String campoAcao) {
		this.nome = nome;
		this.deus = deus;
		this.campoAcao = campoAcao;
	}
}
