package sysRPG.domains;

import java.util.*;

/**
 * Classe representativa de um Deus
 * @author Luke Frozz
 * @since 28/09/2015
 */
public class Deus {
	/**
	 * Id do Deus
	 */
	private Long id;
	
	/**
	 * Nome do Deus
	 */
	private String nome;
	
	/**
	 * Tendencia do Deus
	 */
	private Tendencia tendencia;
	
	/**
	 * Ra�as t�picas de adoradores do Deus
	 */
	private List<Raca> racasTipicas;
	
	/**
	 * Classes t�picas de adoradores do Deus
	 */
	private List<Classe> classesTipicas;
	
	/**
	 * Arma Preferida do Deus
	 */
	private Arma armaPreferida;
	
	/**
	 * Getter de Id
	 * @return Id do Deus
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Setter de Id
	 * @param id Atribui o Id do Deus
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Getter de Nome
	 * @return Nome do Deus
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Setter de Nome
	 * @param nome Atribui o Nome de Deus
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Getter de Tend�ncia
	 * @return Tend�ncia do Deus
	 */
	public Tendencia getTendencia() {
		return tendencia;
	}
	
	/**
	 * Setter de Tend�ncia
	 * @param tendencia Atribui a Tend�ncia do Deus
	 */
	public void setTendencia(Tendencia tendencia) {
		this.tendencia = tendencia;
	}
	
	/**
	 * Getter de Adoradores T�picos
	 * @return Adoradores T�picos do Deus
	 */
	public List<Raca> getRacasTipicas() {
		return racasTipicas;
	}
	
	/**
	 * Setter de Ra�as T�picos
	 * @param adoradoresTipicos Atribui uma lista de Ra�as T�picos
	 * de adoradores do Deus
	 */
	public void setRacasTipicas(List<Raca> racasTipicas) {
		this.racasTipicas = racasTipicas;
	}
	
	/**
	 * Getter de Classes T�picas
	 * @return Classes T�picas
	 * de adoradores do Deus
	 */
	public List<Classe> getClassesTipicas() {
		return classesTipicas;
	}
	
	/**
	 * Setter de Classes T�picas
	 * @param classesTipicas Atribui uma lista de Classes T�picas 
	 * de adoradores do Deus
	 */
	public void setClassesTipicas(List<Classe> classesTipicas) {
		this.classesTipicas = classesTipicas;
	}
	
	/**
	 * Getter de Arma Preferida
	 * @return Arma Preferida do Deus
	 */
	public Arma getArmaPreferida() {
		return armaPreferida;
	}
	
	/**
	 * Setter de Arma Preferida
	 * @param armaPreferida Atribui a Arma Preferida do Deus
	 */
	public void setArmaPreferida(Arma armaPreferida) {
		this.armaPreferida = armaPreferida;
	}
	
	/**
	 * Construtor padr�o do Deus
	 */
	public Deus() {
		this.armaPreferida = new Arma();
	}
	
	/**
	 * Construtor completo de Deus
	 * @param nome atribui o nome do Deus
	 * @param tendencia atribui a Tendencia do Deus
	 * @param racasTipicas atribui as racas tipicas de adora��o do Deus
	 * @param classesTipicas atribui as Classes tipicas de adora��o do Deus
	 * @param armaPreferida atribui a arma preferida do Deus
	 */
	public Deus(String nome, Tendencia tendencia, List<Raca> racasTipicas, List<Classe> classesTipicas, Arma armaPreferida) {
		this.nome = nome;
		this.tendencia = tendencia;
		this.racasTipicas = racasTipicas;
		this.classesTipicas = classesTipicas;
		this.armaPreferida = armaPreferida;
	}
	
	/**
	 * Construtor completo de Deus
	 * @param nome atribui o nome do Deus
	 * @param tendencia atribui a Tendencia do Deus
	 * @param racasTipicas atribui as racas tipicas de adora��o do Deus
	 * @param classesTipicas atribui as Classes tipicas de adora��o do Deus
	 * @param armaPreferida atribui a arma preferida de adora��o do Deus
	 */
	public Deus(String nome, Tendencia tendencia, List<Raca> racasTipicas, List<Classe> classesTipicas, String armaPreferida) {
		this.nome = nome;
		this.tendencia = tendencia;
		this.racasTipicas = racasTipicas;
		this.classesTipicas = classesTipicas;
		this.armaPreferida = new Arma(armaPreferida);
	}

	/**
	 * Construtor simples de Deus
	 * @param nome atribuio nome do Deus
	 * @param tendencia atribui a Tend�ncia do Deus
	 * @param armaPreferida atribui a Arma Preferida do Deus
	 */
	public Deus(String nome, Tendencia tendencia, Arma armaPreferida) {
		this.nome = nome;
		this.tendencia = tendencia;
		this.armaPreferida = armaPreferida;
	}
	
	/**
	 * Construtor simples de Deus
	 * @param nome atribuio nome do Deus
	 * @param tendencia atribui a Tend�ncia do Deus
	 * @param armaPreferida atribui a Arma Preferida do Deus
	 */
	public Deus(String nome, Tendencia tendencia, String armaPreferida) {
		this.nome = nome;
		this.tendencia = tendencia;
		this.armaPreferida = new Arma(armaPreferida);
	}
}

