package sysRPG.domains;
/**
 * Classe Representativa de uma Arma
 * @author Luke Frozz
 * @since 28/09/2015
 */
public class Arma {
	
	/**
	 * Id da Arma
	 */
	private long id;
	
	/**
	 * Nome da Arma
	 */
	private String nome;
	
	/**
	 * Getter de Id
	 * @return Id da Arma
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Setter de Id
	 * @param id Atribui o ID da Arma
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Getter de Nome
	 * @return Nome da Arma
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Setter de Nome
	 * @param nome Atribui o nome da Arma
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Construtor padr�o de Arma
	 */
	public Arma(){
		
	}
	
	/**
	 * Construtor completo de Arma
	 * @param nome atribui o nome da Arma
	 */
	public Arma(String nome) {
		this.nome = nome;
	}
}
