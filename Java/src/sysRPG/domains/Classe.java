package sysRPG.domains;

public class Classe {
	/**
	 * Id da Classe
	 */
	private Long id;
	/**
	 * Nome da Classe
	 */
	private String nome;
	/**
	 * Nomeclatura da Classe
	 */
	private String nomeclatura;
	/**
	 * Descri��o da Classe
	 */
	private String descricao;
	
	/**
	 * Getter de Id
	 * @return Id da Classe
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Setter de Id
	 * @param id atribui o Id da Classe
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * Getter de Nome
	 * @return Nome da Classe
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Setter de Nome
	 * @param nome atribui o Nome da Classe
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Getter de nomeclatura
	 * @return Nomeclatura da Classe
	 */
	public String getNomeclatura() {
		return nomeclatura;
	}
	/**
	 * Setter de nomeclatura
	 * @param nomeclatura Atribui a Nomeclatura da Classe
	 */
	public void setNomeclatura(String nomeclatura) {
		this.nomeclatura = nomeclatura;
	}
	/**
	 * Getter de Descri��o
	 * @return Descri��o da Classe
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Setter de Descri��o
	 * @param descricao atribui a Descri��o da Classe
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Construtor padr�o da Classe
	 */
	public Classe() {
		
	}
}
