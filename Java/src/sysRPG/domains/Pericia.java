/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de Perícia
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Pericia {
	/**
	 * Id da Per�cia
	 */
    public Long id;
    /**
     * Nome da Per�cia
     */
    public String nome;
    /**
     * Descri��o da Per�cia
     */
    public String descricao;
    
    /**
     * Getter de Id
     * @return Id da Per�cia
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id da Per�cia
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome da Per�cia
     */
    public String getNome() {
        return nome;
    }
    /**
     * Setter de Nome
     * @param nome atribui o Nome da Per�cia
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Getter de Descri��o
     * @return Descri��o da Per�cia
     */
    public String getDescricao() {
        return descricao;
    }
    /**
     * Setter de Descri��o
     * @param descricao atribui a Descri��o da Per�cia
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    /**
     * Construtor padr�o de Per�cia
     */
    public Pericia() {
    	
    }
}
