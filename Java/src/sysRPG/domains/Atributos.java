/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de atributos
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Atributos {
	/**
	 * Atributo fisico de Forca
	 */
    private int forca;
    /**
     * Atributo fisico de Destreza
     */
    private int destreza;
    /**
     * Atributo fisico de Constituicao
     */
    private int constituicao;
    /**
     * Atributo mental de Inteligencia
     */
    private int inteligencia;
    /**
     * Atributo mental de Sabedoria
     */
    private int sabedoria;
    /**
     * Atributo mental de Carisma
     */
    private int carisma;

    /**
     * Metodo para identificar qual e o valor
     * do modificador de Habilidade de um Atributo
     * @param attr atributo para a verificacao
     * @return Modificador de Habilidade do Atributo
     */
    public static Integer mod(Integer attr) {
    	int mod = 0;
    	
    	if (attr <= 1)
    		mod = -5;
    	else {
    		if (attr % 2 == 1)
    			mod = ((attr - 1) / 2) - 5;
    		else
    			mod = (attr / 2) - 5;
    	}
    	
    	return mod;
    }

    /**
     * Getter de forca
     * @return FOR
     */
    public int getForca() {
        return forca;
    }
    /**
     * Setter de forca
     * @param forca atribui o valor da Forca
     */
    public void setForca(int forca) {
        this.forca = forca;
    }
    /**
     * Getter de destreza
     * @return DES
     */
    public int getDestreza() {
        return destreza;
    }
    /**
     * Setter de destreza
     * @param destreza atribui o valor da destreza
     */
    public void setDestreza(int destreza) {
        this.destreza = destreza;
    }
    /**
     * Getter de constituicao
     * @return CONS
     */
    public int getConstituicao() {
        return constituicao;
    }
    /**
     * Setter de constituicao
     * @param constituicao atribui o valor de constituicao
     */
    public void setConstituicao(int constituicao) {
        this.constituicao = constituicao;
    }
    /**
     * Getter de inteligencia
     * @return INT
     */
    public int getInteligencia() {
        return inteligencia;
    }
    /**
     * Setter de inteligencia
     * @param inteligencia atribui o valor da inteligencia
     */
    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }
    /**
     * Getter de sabedoria
     * @return SAB
     */
    public int getSabedoria() {
        return sabedoria;
    }
    /**
     * Setter de sabedoria
     * @param sabedoria atribui o valor da sabedoria
     */
    public void setSabedoria(int sabedoria) {
        this.sabedoria = sabedoria;
    }
    /**
     * Getter de carisma
     * @return CAR
     */
    public int getCarisma() {
        return carisma;
    }
    /**
     * Setter de carisma
     * @param carisma atribui o valor do carisma
     */
    public void setCarisma(int carisma) {
        this.carisma = carisma;
    }
    
    /**
     * Construtor padrao de Atributos
     */
    public Atributos() {
    	this.forca = 0;
    	this.destreza = 0;
    	this.constituicao = 0;
    	this.inteligencia = 0;
    	this.sabedoria = 0;
    	this.carisma = 0;
    }
    
    /**
     * Construtor completo de atributos
     * @param forca atribui o valor de forca
     * @param destreza atribui o valor de destreza
     * @param constituicao atribui o valor de constituicao
     * @param inteligencia atribui o valor de inteligencia
     * @param sabedoria atribui o valor de sabedoria
     * @param carisma atribui o valor de carisma
     */
	public Atributos(int forca, int destreza, int constituicao, int inteligencia, int sabedoria, int carisma) {
		this.forca = forca;
		this.destreza = destreza;
		this.constituicao = constituicao;
		this.inteligencia = inteligencia;
		this.sabedoria = sabedoria;
		this.carisma = carisma;
	}
    
    
}
