/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de uma Tendência
 * @author Luke Frozz
 * @since 23/09/2015
 */
public class Tendencia {
	/**
	 * Id da Tendencia
	 */
    private Long id;
    /**
     * Nome em portugues da Tendencia
     */
    private String nomePT;
    /**
     * Nome em ingles da Tendencia
     */
    private String nomeING;
    /**
     * Descricao da Tendencia
     */
    private String descricao;
    /**
     * Indice Moral da Tendencia
     */
    private Moral moral;
    /**
     * Indice Etico da Tendencia
     */
    private Etica etica;

    /**
     * Getter de Id
     * @return Id da Tendencia
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id de Tendencia
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de nomePT
     * @return nome em portugues da Tendencia
     */
    public String getNomePT() {
        return nomePT;
    }
    /**
     * Setter de nomePT
     * @param nomePT atribui o nome em Portugues de Tendencia
     */
    public void setNomePT(String nomePT) {
        this.nomePT = nomePT;
    }
    /**
     * Getter de nomeING
     * @return Nome em Ingles de Tendencia
     */
    public String getNomeING() {
        return nomeING;
    }
    /**
     * Setter de nomeING
     * @param nomeING atribui o nome em Ingles de Tendencia
     */
    public void setNomeING(String nomeING) {
        this.nomeING = nomeING;
    }
    /**
     * Getter de Descricao
     * @return Descricao de Tendencia
     */
    public String getDescricao() {
        return descricao;
    }
    /**
     * Setter de Descricao
     * @param descricao atribui a Descricao da Tendencia
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    /**
     * Getter de Moral
     * @return Eixo Moral da Tendencia, essa que pode ser: <br/>
     * <ol>
     * 	  <li>Leal</li>
     * 	  <li>Neutro</li>
     * 	  <li>Caotico</li>
     * </ol>
     */
    public Moral getMoral() {
        return moral;
    }
    
    /**
     * Setter de Moral
     * @param moral atribui o Eixo Moral da Tendencia, essa que pode ser:<br/>
     * <ol>
     * 	  <li>Leal</li>
     * 	  <li>Neutro</li>
     * 	  <li>Caotico</li>
     * </ol>
     */
    public void setMoral(Moral moral) {
        this.moral = moral;
    }

    /**
     * Getter de Etica
     * @return Eixo etico da Tendencia, essa que pode ser: <br/>
     * <ol>
     * 	  <li>Bom</li>
     * 	  <li>Neutro</li>
     * 	  <li>Mal</li>
     * </ol>
     */
    public Etica getEtica() {
        return etica;
    }
    /**
     * Setter de Etica
     * @param etica atribui o Eixo Etico da Tendencia, esse que pode ser: <br/>
     * <ol>
     * 	  <li>Bom</li>
     * 	  <li>Neutro</li>
     * 	  <li>Mal</li>
     * </ol>
     */
    public void setEtica(Etica etica) {
        this.etica = etica;
    }
    
    /**
     * Construtor padrao de Tendencia
     */
    public Tendencia() {
    	
    }
    
    /**
     * Construtor completo de tendencia
     * para a atribuicao direta dos
     * eixos de Moral e Etica
     * @param moral atribui o Eixo Moral da Tendencia
     * @param etica atribui o Eixo Etico da Tendencia
     */
	public Tendencia(Moral moral, Etica etica) {;
		this.moral = moral;
		this.etica = etica;
	}

}
