package sysRPG.domains;

/**
 * Enumerador de Moral
 * @author Luke Frozz
 * @since 29/09/2015
 */
public enum Moral {
	Leal,
	Neutro,
	Caotico
}
