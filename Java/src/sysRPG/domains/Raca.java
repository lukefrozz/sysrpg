/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representativa de Ra�a
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Raca {
	/**
	 * Id da Raca
	 */
    private Long id;
    /**
     * Nome da Raca
     */
    private String Nome;
    /**
     * Descricao da Raca
     */
    private String Descricao;
    /**
     * Tendencia padrao da Raca
     */
    private Tendencia tendenciaPadrao;
    /**
     * Tamanho Padrao da Raca
     */
    private Tamanho tamanhoPadrao;
    /**
     * Alteradores de Atributos da Raca
     */
    private Atributos alteracaoAtributos;
    /**
     * Alteradores de Pericia da Raca
     */
    private List<SlotPericia> alteracaoPericias;
    /**
     * Atributo que quantifica o n�mero de 
     * valores customizaveis em uma ra�a caso seus atributos
     * modificadores sejam customiz�veis
     */
    private Integer qtdCustom;
	/**
     * Getter de Id
     * @return Id da Raca
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id da Raca
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome da Raca
     */
    public String getNome() {
        return Nome;
    }
    /**
     * Setter de Nome
     * @param Nome atribui o Nome da Raca
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    /**
     * Getter de Descricao
     * @return Descricao da Raca
     */
    public String getDescricao() {
        return Descricao;
    }
    /**
     * Setter de Descricao
     * @param Descricao atribui a Descricao da Raca
     */
    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }
    /**
     * Getter de tendenciaPadrao
	 * @return Tendencia padrao da Raca
	 */
	public Tendencia getTendenciaPadrao() {
		return tendenciaPadrao;
	}
	/**
	 * Setter de tendenciaPadrao
	 * @param tendenciaPadrao atribui a Tendencia padr�ao da Raca
	 */
	public void setTendenciaPadrao(Tendencia tendenciaPadrao) {
		this.tendenciaPadrao = tendenciaPadrao;
	}
	/**
	 * Getter de tamanhoPadrao
	 * @return Tamanho padrao da Raca
	 */
	public Tamanho getTamanhoPadrao() {
		return tamanhoPadrao;
	}
	/**
	 * Setter de tamanhoPadrao
	 * @param tamanhoPadrao atribui o Tamanho padrao da Raca
	 */
	public void setTamanhoPadrao(Tamanho tamanhoPadrao) {
		this.tamanhoPadrao = tamanhoPadrao;
	}
	/**
     * Getter de alteracaoAtributos
     * @return Alteradores de Atributos da Raca
     */
    public Atributos getAlteracaoAtributos() {
        return alteracaoAtributos;
    }
    /**
     * Setter de alteracaoAtributos
     * @param alteracaoAtributos atribui os alteradores de Atributos da Raca
     */
    public void setAlteracaoAtributos(Atributos alteracaoAtributos) {
        this.alteracaoAtributos = alteracaoAtributos;
    }
    /**
     * Getter de alteracaoPericias
     * @return Alteradores de Pericia da Raca
     */
    public List<SlotPericia> getAlteracaoPericias() {
        return alteracaoPericias;
    }
    /**
     * Setter de alteracaoPericias
     * @param alteracaoPericias atribui os alteradores de Pericia da Raca
     */
    public void setAlteracaoPericias(List<SlotPericia> alteracaoPericias) {
        this.alteracaoPericias = alteracaoPericias;
    }
    
    /**
     * Getter de qtdCustom
     * @return quantidade customiz�vel de atributos de uma ra�a
     */
    public Integer getQtdCustom() {
		return qtdCustom;
	}
    /**
     * Setter de qtdCustom
     * @param qtdCustom atribui a quantidade customiz�vel de uma ra�a
     */
	public void setQtdCustom(Integer qtdCustom) {
		this.qtdCustom = qtdCustom;
	}
	
	/**
     * Coonstrutor Padrao de Raca
     */
    public Raca(){
    	tendenciaPadrao = new Tendencia();
    	alteracaoAtributos = new Atributos();
    	alteracaoPericias = new ArrayList<>();
    	this.qtdCustom = 0;
    }
    
    /**
     * Construtor completo de raca
     * @param nome atribui o nome da Raca
     * @param tamanhoPadrao atribui o tamanho padrao da Raca
     * @param tendenciaPadrao atribui a tendencia Padrao da Raca
     * @param alteracaoAtributos atribui os alteradores de atributos da Raca
     */
    public Raca(String nome, Tamanho tamanhoPadrao, Tendencia tendenciaPadrao, Atributos alteracaoAtributos, int qtdCustom) {
		Nome = nome;
		this.tendenciaPadrao = tendenciaPadrao;
		this.tamanhoPadrao = tamanhoPadrao;
		this.alteracaoAtributos = alteracaoAtributos;
		this.alteracaoPericias = new ArrayList<>();
		this.qtdCustom = qtdCustom;
	}
    
}
