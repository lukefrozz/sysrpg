package sysRPG.domains;
/**
 * Enumerador de Etica
 * @author Luke Frozz
 * @since 29/09/2015
 */
public enum Etica {
	Bom,
	Neutro,
	Mal
}
