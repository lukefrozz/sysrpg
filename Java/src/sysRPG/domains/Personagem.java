/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Representativa de uma Personagem
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Personagem {
	/**
	 * Id da Personagem
	 */
    private Long id;
    /**
     * Nome da Personagem
     */
    private String nome;
    /**
     * Classe da Personagem
     */
    private List<Classe> classes;
    /**
     * Raça da Personagem
     */
    private Raca raca;
    /**
     * Idade da Personagem
     */
    private Integer idade;
    /**
     * Sexo da Personagem
     */
    private Sexo sexo;
    /**
     * Tamanho da Personagem
     */
    private Tamanho tamanho;
    /**
     * Dividade a qual a Personagem segue
     */
    private Divindade divindade;
    /**
     * Atributos da Personagem
     */
    private Atributos atributos;
    /**
     * Per�cias da Personagem
     */
    private List<SlotPericia> pericias;
    /**
     * Tend�ncia da Personafem
     */
    private Tendencia tendencia;

    /**
     * Getter de Id
     * @return Id da Personagem
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id da Personagem
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de Nome
     * @return Nome da Personagem
     */
    public String getNome() {
        return nome;
    }
    /**
     * Setter de Nome
     * @param nome atribui o Nome da Personagem
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Setter de Classes
     * @return Classes da Personagem
     */
    public List<Classe> getClasses() {
		return classes;
	}
    /**
     * Setter de Classes
     * @param classe atribui as classes da Personagem
     */
	public void setClasses(List<Classe> classe) {
		this.classes = classe;
	}
	/**
	 * Método de adição de Classe as classes da Personagem
	 */
	public void addClasse(Classe classe) {
		this.classes.add(classe);
	}
	/**
     * Getter de Ra�a
     * @return Ra�a da Personagem
     */
    public Raca getRaca() {
        return raca;
    }
    /**
     * Setter de Ra�a
     * @param raca atribui a Ra�a da Personagem
     */
    public void setRaca(Raca raca) {
        this.raca = raca;
        this.tendencia = raca.getTendenciaPadrao();
        this.tamanho = raca.getTamanhoPadrao();
    }
	/**
     * Getter de Idade
     * @return Idade da Personagem
     */
    public Integer getIdade() {
        return idade;
    }
    /**
     * Setter de Idade
     * @param idade atribui a Idade da Personagem
     */
    public void setIdade(Integer idade) {
        this.idade = idade;
    }
    /**
     * Getter de Sexo
     * @return Sexo da Personagem
     */
    public Sexo getSexo() {
        return sexo;
    }
    /**
     * Setter de Sexo
     * @param sexo atribui o Sexo da Personagem
     */
    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }
    /**
     * Getter de Tamanho
	 * @return Tamanho da Personagem
	 */
	public Tamanho getTamanho() {
		return tamanho;
	}
	/**
	 * Setter de Tamanho
	 * @param tamanho atribui o Tamanho da Personagem
	 */
	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	/**
	 * Getter de Divindade
	 * @return Divindade a qual a Personagem segue
	 */
    public Divindade getDivindade() {
		return divindade;
	}
    /**
     * Setter de Divindade
     * @param divindade atribui a Divindade a qual
     * a Personagem segue
     */
	public void setDivindade(Divindade divindade) {
		this.divindade = divindade;
	}
	/**
     * Getter de Atributos
     * @return atribui os Atributos da Personagem
     */
    public Atributos getAtributos() {
        return atributos;
    }
    /**
     * Setter de Atributos
     * @param atributos atribui os Atributos da Personagem
     */
    public void setAtributos(Atributos atributos) {
        this.atributos = atributos;
    }
    /**
     * Getter de Per�cias
     * @return Per�cias da Personagem
     */
    public List<SlotPericia> getPericias() {
        return pericias;
    }
    /**
     * Setter de Per�cias
     * @param pericias atribui as Per�cias da Personagem
     */
    public void setPericias(List<SlotPericia> pericias) {
        this.pericias = pericias;
    }
    /**
     * Getter de Tend�ncia
     * @return Tend�ncia da Personagem
     */
    public Tendencia getTendencia() {
        return tendencia;
    }
    /**
     * Setter de Tend�ncia
     * @param tendencia atribui a Tend�ncia do Personagem
     */
    public void setTendencia(Tendencia tendencia) {
        this.tendencia = tendencia;
    }
    
    /**
     * Construtor padr�o de Personagem
     */
    public Personagem() {
    	this.id = Long.parseLong("0");
    	this.idade = 0;
    	this.raca = new Raca();
    	this.divindade = new Divindade();
    	this.tamanho = Tamanho.M;
    	this.atributos = new Atributos();
    	this.pericias = new ArrayList<>();
    	this.tendencia = new Tendencia();
    	this.classes = new ArrayList<>();
    }
    
}
